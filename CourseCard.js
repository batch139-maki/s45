import {Container, Col, Row, Column, Card} from "react-bootstrap"

export default function CourseCard() {

	return( 
		
		<Container fluid className="mb-4">
			<Row >
			{/*card 1*/}
				<Col xs={12} md={12}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Sample Course 1</Card.Title>
					    <Card.Text>
					     <p>Description:
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. </p>

					     <p> Price: 
					      Php 40,000</p>
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			{/*card 2*/}
				<Col xs={12} md={12}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Sample Course 2</Card.Title>
					    <Card.Text>
					      <p>Description:
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. </p>

					     <p> Price: 
					      Php 40,000</p>
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			{/*card 3*/}
				<Col xs={12} md={12}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Sample Course 3</Card.Title>
					    <Card.Text>
					      <p>Description:
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content. </p>

					     <p> Price: 
					      Php 40,000</p>
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}