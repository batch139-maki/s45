import {Container, Col, Row, Column, Card} from "react-bootstrap"

export default function Highlights() {

	return( 
		
		<Container fluid className="mb-4">
			<Row >
			{/*card 1*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			{/*card 2*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Study Now, Pay Later!</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			{/*card 3*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Be Part of our Community</Card.Title>
					    <Card.Text>
					      Some quick example text to build on the card title and make up the bulk of
					      the card's content.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}